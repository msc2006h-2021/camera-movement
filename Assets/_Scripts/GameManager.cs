﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using Cinemachine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    private CanvasGroup canvas;
    private float fadeDuration = 0.25f;
    private IEnumerator C_Fade;
    private IEnumerator C_Load;

    private void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        canvas = GetComponent<CanvasGroup>();
    }

    public void OpenMenu()
    {
        FirstPersonController fpc = FindObjectOfType<FirstPersonController>();
        if(fpc != null)
        {
            fpc.gameObject.SetActive(false);
        }
        FadeIn();
    }

    public void CloseMenu()
    {
        FadeOut();
    }

    public void LoadScene(string scene)
    {
        if(C_Load != null)
        {
            StopCoroutine(C_Load);
        }
        C_Load = LoadLevel(scene);
        StartCoroutine(C_Load);

        //override Cinemachine FreeLook properties for orbit scene;
        if(scene == "OrbitCam")
        {
            CinemachineCore.GetInputAxis = GetAxisOnMouseDown;
        }
        else
        {
            CinemachineCore.GetInputAxis = DefaultAxis;
        }
    }

    public float DefaultAxis(string axisName)
    {
        return Input.GetAxis(axisName);
    }

    public float GetAxisOnMouseDown(string axisName)
    {
        //Check if left mouse button is down...
        if (Input.GetMouseButton(0))
            return Input.GetAxis(axisName); //...then return input axis values
        return 0; //otherwise, make it seem like the mouse isn't moving

    }

    public IEnumerator LoadLevel(string scene)
    {
        AsyncOperation asyncLoadLevel = SceneManager.LoadSceneAsync(scene);
        while (!asyncLoadLevel.isDone)
            yield return null;
        yield return new WaitForEndOfFrame();
        FadeOut();
    }

    private void FadeIn()
    {
        if(C_Fade != null)
        {
            StopCoroutine(C_Fade);
        }
        C_Fade = Fade(1f, true);
        StartCoroutine(C_Fade);
    }

    private void FadeOut()
    {
        if (C_Fade != null)
        {
            StopCoroutine(C_Fade);
        }
        C_Fade = Fade(0f, false);
        StartCoroutine(C_Fade);
    }

    private IEnumerator Fade(float to, bool interactable)
    {
        float startTime = Time.time;
        float startAlpha = canvas.alpha;
        while(Time.time - startTime < fadeDuration)
        {
            canvas.alpha = Mathf.Lerp(startAlpha, to, (Time.time - startTime) / fadeDuration);
            yield return null;
        }
        canvas.alpha = to;
        canvas.interactable = interactable;
        canvas.blocksRaycasts = interactable;
    }

}
