﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


public class CMFreelookOverride : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //overrride CM core polling Input manager for axis values
        CinemachineCore.GetInputAxis = GetAxisOnMouseDown;
    }

    public float GetAxisOnMouseDown(string axisName)
    {
        //Check if left mouse button is down...
        if (Input.GetMouseButton(0))
            return Input.GetAxis(axisName); //...then return input axis values
        return 0; //otherwise, make it seem like the mouse isn't moving
        
    }
}
