﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackController : MonoBehaviour
{
    public void OpenMenu()
    {
        if(GameManager.Instance != null)
        {
            GameManager.Instance.OpenMenu();
        }
    }

    public void CloseMenu()
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.CloseMenu();
        }
    }
}
