﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderCameraFrustum : MonoBehaviour
{
    private Camera _cam;
    private LineRenderer _line;

    // Start is called before the first frame update
    void Start()
    {
        _cam = Camera.main;
        _line = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3[] frustumCorners = new Vector3[4];
        _cam.CalculateFrustumCorners(new Rect(0, 0, 1, 1), _cam.farClipPlane, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);
        Vector3[] linePositions = new Vector3[8];
        linePositions[0] = _cam.transform.TransformVector(frustumCorners[0]);
        for(int i = 1; i < 8; i++)
        {
            if(i%2==1)
            {
                linePositions[i] = _cam.transform.position;
            }
            else
            {
                linePositions[i] = _cam.transform.TransformVector(frustumCorners[i / 2]);
            }
        }
        _line.SetPositions(linePositions);
    }
}
