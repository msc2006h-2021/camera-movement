﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    //public variables
    public Transform cameraTransform; //this is the transform component attached to the Main Camera
    public Image storyboardImage; //the UI image in the scene we will be updating
    public float cameraSpeed = 0.5f;

    //variables set by the event triggers on the camera controll buttons
    [Header ("Camera Controls")]
    public bool tiltUp;
    public bool tiltDown;
    public bool panRight;
    public bool panLeft;

    public bool pedestalUp;
    public bool pedestalDown;
    public bool trackRight;
    public bool trackLeft;

    public bool dollyIn;
    public bool dollyOut;

    public bool zoom;
    

    //images to show for each action
    [Header("Storyboard Sprites")]
    public Sprite tiltUpSprite;
    public Sprite tiltDownSprite;
    public Sprite panRightSprite;
    public Sprite panLeftSprite;

    public Sprite pedestalUpSprite;
    public Sprite pedestalDownSprite;
    public Sprite trackRightSprite;
    public Sprite trackLeftSprite;

    public Sprite dollyInSprite;
    public Sprite dollyOutSprite;
    public Sprite zoomInSprite;
    public Sprite zoomOutSprite;
    public Sprite blankSprite;

    //private variables
    private Camera _cam;
    private Vector3 _position;
    private float _fov;
    private float zoomValue;
    private float oldZoom;

    //This gets called only once, when our script initializes
    private void Start()
    {
        _cam = Camera.main;
        _fov = _cam.fieldOfView;
        _position = cameraTransform.position;
    }

    //The main update loop of our script. This is called once every frame
    private void Update()
    {
        //check the values of all the bools that respond to button presses
        //while a button is pressed, call a rotate, move, or zoom function
        if(tiltUp) 
        {
            RotateCamera(cameraTransform.right*-1); //predefinded vector with the value of (-1,0,0)
            ChangeSprite(tiltUpSprite); //update the UI
            return;
        }
        if (tiltDown)
        {
            RotateCamera(cameraTransform.right); //predefinded vector with the value of (1,0,0)
            ChangeSprite(tiltDownSprite); //update the UI
            return;
        }
        if (panRight)
        {
            RotateCamera(cameraTransform.up); //predefinded vector with the value of (0,1,0)
            ChangeSprite(panRightSprite); //update the UI
            return;
        }
        if (panLeft)
        {
            RotateCamera(cameraTransform.up*-1); //predefinded vector with the value of (0,-1,0)
            ChangeSprite(panLeftSprite); //update the UI
            return;
        }
        if (pedestalUp)
        {
            MoveCamera(cameraTransform.up);
            ChangeSprite(pedestalUpSprite);
            return;
        }
        if (pedestalDown)
        {
            MoveCamera(cameraTransform.up * -1);
            ChangeSprite(pedestalDownSprite);
            return;
        }
        if (trackRight)
        {
            MoveCamera(cameraTransform.right);
            ChangeSprite(trackRightSprite);
            return;
        }
        if (trackLeft)
        {
            MoveCamera(cameraTransform.right*-1);
            ChangeSprite(trackLeftSprite);
            return;
        }
        if (dollyIn)
        {
            MoveCamera(cameraTransform.forward);
            ChangeSprite(dollyInSprite);
            return;
        }
        if (dollyOut)
        {
            MoveCamera(cameraTransform.forward * -1);
            ChangeSprite(dollyOutSprite);
            return;
        }
        if (zoom)
        {
            if (zoomValue < oldZoom)
            {
                ChangeSprite(zoomInSprite);
            }
            if (zoomValue > oldZoom)
            {
                ChangeSprite(zoomOutSprite);
            }
            return;
        }

        ChangeSprite(blankSprite); //clear UI

    }

    //rotates the camera transform in the x,y direction provided
    private void RotateCamera(Vector3 direction)
    {
        cameraTransform.Rotate(direction * cameraSpeed * 20f, Space.World);
    }

    //moves the camera transform in the x,y direction provided
    private void MoveCamera(Vector3 direction)
    {
        cameraTransform.Translate(direction * cameraSpeed, Space.World);
    }

    //changes the field of view of the camera by the value provided
    public void ZoomCamera(float value)
    {
        oldZoom = zoomValue;
        zoomValue = value;

        _cam.fieldOfView = value;
    }

    private void ChangeSprite(Sprite sprite)
    {
        if(storyboardImage.sprite != sprite)
        {
            storyboardImage.sprite = sprite;
        }    
    }

    //public functions called by Event Triggers on the camera control buttons
    public void TiltUp(bool pressed)
    {
        tiltUp = pressed;
    }

    public void TiltDown(bool pressed)
    {
        tiltDown = pressed;
    }

    public void PanRight(bool pressed)
    {
        panRight = pressed;
    }

    public void PanLeft(bool pressed)
    {
        panLeft = pressed;
    }

    public void DollyIn(bool pressed)
    {
        dollyIn = pressed;
    }

    public void DollyOut(bool pressed)
    {
        dollyOut = pressed;
    }

    public void PedestalUp(bool pressed)
    {
        pedestalUp = pressed;
    }

    public void PedestalDown(bool pressed)
    {
        pedestalDown = pressed;
    }

    public void TrackRight(bool pressed)
    {
        trackRight = pressed;
    }

    public void TrackLeft(bool pressed)
    {
        trackLeft = pressed;
    }

    public void Zoom(bool pressed)
    {
        zoom = pressed;
    }

    public void Reset()
    {
        cameraTransform.position = _position;
        cameraTransform.rotation = Quaternion.identity;
        _cam.fieldOfView = _fov;
    }
}
